PYGTFS_VENV_DIR ?= ~/.envs/pygtfs

all: init install

init:
	uv venv --quiet --seed $(PYGTFS_VENV_DIR)

install:
	uv pip install --editable '.[dev,test]'

destroy:
	rm -rf $(PYGTFS_VENV_DIR)

clean: destroy init

refresh: clean install

format:
	ruff format

lint:
	ruff check --fix

isort:
	ruff check --select I --fix

pretty: isort format

typecheck:
	mypy src/

check: pretty lint typecheck

test:
	$(PYGTFS_VENV_DIR)/bin/pytest

test_coverage:
	$(PYGTFS_VENV_DIR)/bin/python -m coverage run -m pytest

coverage: test_coverage
	$(PYGTFS_VENV_DIR)/bin/python -m coverage report
