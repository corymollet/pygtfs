[project]
name = "pygtfs"
description = "Python library for working with GTFS data"
authors = [
    {name = "Cory Mollet", email="contact@corymollet.com"},
]
version = "0.1.0"
readme = "README.rst"
classifiers = [
    "Development Status :: 3 - Alpha",
    "Intended Audience :: Developers",
    "License :: OSI Approved :: MIT License",
    "Topic :: Database",
    "Topic :: File Formats :: JSON :: JSON Schema",
    "Topic :: Scientific/Engineering :: GIS",
    "Topic :: Software Development :: Libraries",
]
dependencies = [
    "pydantic[email]",
]

[project.optional-dependencies]
dev = [
    "faker",
    "mypy",
    "pre-commit",
    "pre-commit-hooks",
    "ruff",
]
docs = []
test = [
    "coverage",
    "pytest",
]

[project.urls]
Homepage = "https://gitlab.com/corymollet/pygtfs"
Repository = "https://gitlab.com/corymollet/pygtfs/"
Issues = "https://gitlab.com/corymollet/pygtfs/-/issues"

[build-system]
requires = ["setuptools"]
build-backend = "setuptools.build_meta"

[tool.coverage.run]
source = ["src", "tests"]

[tool.mypy]
mypy_path = "./src"
warn_return_any = true
warn_unused_configs = true

[tool.pytest]
norecursedirs = [
    ".git",
    ".tox",
    ".env",
    ".mypy_cache",
    "dist",
    "build",
    "migrations",
]
python_files = [
    "test_*.py",
    "*_test.py",
    "tests.py",
]
adopts = [
    "-ra",
    "--strict-markers",
    "--ignore=docs/conf.py",
    "--ignore=ci",
    "--ignore=.eggs",
    "--doctest-module",
    "--doctest-glob=*.rst",
    "--tb=short",
]
testpaths = [
    "tests",
]
# https://til.simonwillison.net/pytest/treat-warnings-as-errors
filterwarnings = [
    "error",
]

[tool.ruff]
extend-exclude = ["static", "ci/templates"]
fix = true
line-length = 140
src = ["src", "tests"]
target-version = "py310"
[tool.ruff.format]
docstring-code-format = true
docstring-code-line-length = 60
[tool.ruff.lint]
ignore = [
    "E501", # pycodestyle line-too-long
    "COM812", # flake8 missing-trailing-comma
]
select = [
    "ARG", # flake8-unused-arguments
    "B", # flake8-bugbear
    "C4", # flake8-comprehensions
    "C90", # mccabe
    "COM", # flake8-commas
    "DTZ", # flake8-datetimez
    "E", # pycodestyle errors
    "ERA", # eradicate
    "EM", # flake8-errmsg
    "EXE", # flake8-executable
    "F", # pyflakes
    "FIX", # flake8-fixme
    "I", # isort
    "INT", # flake8-gettext
    "N", # pep8-naming
    "PIE", # flake8-pie
    "PLC", # pylint convention
    "PLE", # pylint errors
    "PT", # flake8-pytest-style
    "PTH", # flake8-use-pathlib
    "Q", # flake8-quotes
    "R", # refactor
    "RET", # flake8-return
    "RSE", # flake8-raise
    "RUF", # ruff-specific rules
    "S", # flake8-bandit
    "SIM", # flake8-simplify
    "T20", # flake8-print
    "TD", # flake8-todos
    "TRY", # tryceratops
    "UP", # pyupgrade
    "W", # pycodestyle warnings
]
[tool.ruff.lint.flake8-annotations]
mypy-init-return = true
[tool.ruff.lint.flake8-pytest-style]
fixture-parentheses = false
mark-parentheses = false
[tool.ruff.lint.isort]
forced-separate = ["conftest"]
[tool.ruff.lint.per-file-ignores]
"ci/*" = ["S"]

