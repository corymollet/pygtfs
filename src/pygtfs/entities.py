from datetime import date, time
from enum import Enum
from typing import Self
from uuid import UUID

from pydantic import AnyUrl, BaseModel, EmailStr, Field, NonNegativeFloat, NonNegativeInt, PositiveFloat, PositiveInt, model_validator

Identifier = str | int | UUID
OptionalIdentifier = Identifier | None


class Agency(BaseModel):
    """Transit agencies with service represented in this dataset."""

    agency_id: Identifier
    agency_name: str
    agency_url: AnyUrl
    agency_timezone: str
    agency_lang: str = ""
    agency_phone: str = ""
    agency_fare_url: AnyUrl | None = None
    agency_email: EmailStr | None = None


class LocationType(Enum):
    STOP = 0
    STATION = 1
    ENTRANCE_EXIT = 2
    GENERIC_NODE = 3
    BOARDING_AREA = 4


class WheelchairAccessible(Enum):
    UNKNOWN = 0
    POSSIBLE = 1
    IMPOSSIBLE = 2


class Stop(BaseModel):
    """Stops where vehicles pick up or drop off riders. Also defines stations and station entrances."""

    stop_id: Identifier
    stop_code: str = ""
    stop_name: str = ""
    tts_top_name: str = ""
    stop_desc: str = ""
    stop_lat: float | None = Field(
        default=None,
        ge=-90,
        le=90,
    )
    stop_lon: float | None = Field(
        default=None,
        ge=-180,
        le=180,
    )
    zone_id: str = ""
    stop_url: AnyUrl | None = None
    location_type: LocationType = LocationType.STOP
    parent_station: OptionalIdentifier = None
    stop_timezone: str = ""
    wheelchair_boarding: WheelchairAccessible = WheelchairAccessible.UNKNOWN
    level_id: OptionalIdentifier = None
    platform_code: str = ""

    @model_validator(mode="after")
    def check_stop_name(self) -> Self:
        if self.location_type in (LocationType.STOP, LocationType.STATION, LocationType.ENTRANCE_EXIT) and not self.stop_name.strip():
            msg = f"stop_name must be defined if location_type is {self.location_type}"
            raise ValueError(msg)
        return self

    @model_validator(mode="after")
    def check_lat_lon(self) -> Self:
        if self.location_type in (LocationType.STOP, LocationType.STATION, LocationType.ENTRANCE_EXIT) and (
            self.stop_lat is None or self.stop_lon is None
        ):
            msg = f"stop_lat and stop_lon must be defined if location_type is {self.location_type}"
            raise ValueError(msg)
        return self

    @model_validator(mode="after")
    def check_parent_station(self) -> Self:
        if (
            self.location_type in (LocationType.ENTRANCE_EXIT, LocationType.GENERIC_NODE, LocationType.BOARDING_AREA)
            and not self.parent_station
        ):
            msg = f"parent_station must be defined if location_type is {self.location_type}"
            raise ValueError(msg)
        if self.location_type == LocationType.STATION and self.parent_station:
            msg = f"parent_station cannot be defined if location_type is {self.location_type}"
            raise ValueError(msg)
        return self


class RouteType(Enum):
    TRAM_STREETCAR_LIGHT_RAIL = 0
    SUBWAY = 1
    RAIL = 2
    BUS = 3
    FERRY = 4
    CABLE_TRAM = 5
    AERIAL_LIFT = 6
    FUNICULAR = 7
    TROLLEYBUS = 11
    MONORAIL = 12


class ContinuousPickupDropOff(Enum):
    CONTINUOUS_STOPPING = 0
    NO_CONTINUOUS_STOPPING = 1
    MUST_CONTACT_AGENCY = 2
    MUST_COORDINATE_WITH_DRIVER = 3


class PickupDropOff(Enum):
    REGULARLY_SCHEDULED = 0
    NOT_AVAILABLE = 1
    MUST_CONTACT_AGENCY = 2
    MUST_COORDINATE_WITH_DRIVER = 3


class Route(BaseModel):
    """Transit routes. A route is a group of trips that are displayed to riders as a single service."""

    route_id: Identifier = Field(description="Identifies a route.")
    agency_id: OptionalIdentifier = None
    route_short_name: str = ""
    route_long_name: str = ""
    route_desc: str = ""
    route_type: RouteType
    route_url: str = ""
    route_color: str = Field(
        default="FFFFFF",
        pattern=r"[0-9A-Fa-f]{6}",
    )
    route_text_color: str = Field(
        default="000000",
        pattern=r"[0-9A-Fa-f]{6}",
    )
    route_sort_order: NonNegativeInt | None = None
    continuous_pickup: ContinuousPickupDropOff | None = None
    continuous_drop_off: ContinuousPickupDropOff | None = None
    network_id: OptionalIdentifier = None

    @model_validator(mode="after")
    def check_short_long_names(self) -> Self:
        if not self.route_short_name.strip() and not self.route_long_name.strip():
            msg = "At least one of route_short_name or route_long_name must be defined"
            raise ValueError(msg)
        return self


class Direction(Enum):
    OUTBOUND = 0
    INBOUND = 1


class BikesAllowed(Enum):
    UNKNOWN = 0
    CAN_ACCOMMODATE = 1
    NOT_ALLOWED = 2


class Trip(BaseModel):
    """Trips for each route. A trip is a sequence of two or more stops that occur during a specific time period."""

    route_id: Identifier
    service_id: Identifier
    trip_id: Identifier
    trip_headsign: str = ""
    trip_short_name: str = ""
    direction_id: Direction | None = None
    block_id: str | None = None
    shape_id: OptionalIdentifier = None
    wheelchair_accessible: WheelchairAccessible = WheelchairAccessible.UNKNOWN
    bikes_allowed: BikesAllowed = BikesAllowed.UNKNOWN


class TimePoint(Enum):
    APPROXIMATE = 0
    EXACT = 1


class StopTime(BaseModel):
    """Times that a vehicle arrives at and departs from stops for each trip."""

    trip_id: Identifier = Field(description="Foreign ID referencing trips.trip_id.")
    arrival_time: time | None = None
    departure_time: time | None = Field(default=None)
    stop_id: OptionalIdentifier = Field(default=None)
    location_group_id: OptionalIdentifier = Field(default=None)
    location_id: OptionalIdentifier = Field(default=None)
    stop_sequence: str = Field(default="")
    stop_headsign: str = Field(default="")
    start_pickup_drop_off_window: time | None = Field(default=None)
    end_pickup_drop_off_window: time | None = Field(default=None)
    pickup_type: PickupDropOff | None = Field(default=None)
    drop_off_type: PickupDropOff | None = Field(default=None)
    continuous_pickup: ContinuousPickupDropOff | None = Field(default=None)
    continuous_drop_off: ContinuousPickupDropOff | None = Field(default=None)
    shape_dist_traveled: NonNegativeFloat | None = Field(default=None)
    timepoint: TimePoint | None = Field(default=None)
    pickup_booking_rule_id: str | None = Field(default=None, description="ID referencing booking_rules.booking_rule_id")
    drop_off_booking_rule_id: str | None = Field(default=None, description="ID referencing booking_rules.booking_rule_id")

    @model_validator(mode="after")
    def check_timepoint(self) -> Self:
        if self.timepoint == TimePoint.EXACT and (self.arrival_time is None or self.departure_time is None):
            msg = f"arrival/departure_time fields must be defined if timepoint is {self.timepoint}"
            raise ValueError(msg)
        return self

    @model_validator(mode="after")
    def check_time_fields(self) -> Self:
        if (self.start_pickup_drop_off_window is not None and self.end_pickup_drop_off_window is None) or (
            self.end_pickup_drop_off_window is not None and self.start_pickup_drop_off_window is None
        ):
            msg = "Both start_pickup_drop_off_window and end_pickup_drop_off_window must be defined if one of them is defined."
            raise ValueError(msg)
        if (self.arrival_time is not None or self.departure_time is not None) and (
            self.start_pickup_drop_off_window is not None or self.end_pickup_drop_off_window is not None
        ):
            msg = "arrival/departure_time fields are mutually exclusive with start/end pickup_drop_off_window fields."
            raise ValueError(msg)
        return self

    @model_validator(mode="after")
    def check_pickup_drop_off_type(self) -> Self:
        if (
            self.pickup_type in (PickupDropOff.REGULARLY_SCHEDULED, PickupDropOff.MUST_COORDINATE_WITH_DRIVER)
            or self.drop_off_type
            in (
                PickupDropOff.REGULARLY_SCHEDULED,
                PickupDropOff.MUST_COORDINATE_WITH_DRIVER,
            )
        ) and (self.start_pickup_drop_off_window is not None or self.end_pickup_drop_off_window is not None):
            msg = f"pickup_type cannot be {self.pickup_type} if start_pickup_drop_off_window or end_pickup_drop_off_window are defined"
            raise ValueError(msg)
        return self

    @model_validator(mode="after")
    def check_continuous_drop_off(self) -> Self:
        if self.continuous_drop_off is not None and (
            self.start_pickup_drop_off_window is not None or self.end_pickup_drop_off_window is not None
        ):
            msg = "continuous_drop_off cannot be defined if start_pickup_drop_off_window or end_pickup_drop_off_window are defined"
            raise ValueError(msg)
        return self


class CalendarAvailability(Enum):
    UNAVAILABLE = 0
    AVAILABLE = 1


class Calendar(BaseModel):
    """Service dates specified using a weekly schedule with start and end dates."""

    service_id: str
    monday: CalendarAvailability
    tuesday: CalendarAvailability
    wednesday: CalendarAvailability
    thursday: CalendarAvailability
    friday: CalendarAvailability
    saturday: CalendarAvailability
    sunday: CalendarAvailability
    start_date: date = Field(description="Start service day for the service interval.")
    end_date: date = Field(description="End service day for the service interval (inclusive).")


class ExceptionType(Enum):
    ADDED = 1
    REMOVED = 2


class CalendarDate(BaseModel):
    """Exceptions for the services defined in the calendar.txt."""

    service_id: Identifier = Field(description="Identifies a set of dates when a service exception occurs for one or more routes.")
    date_: date = Field(description="Date when service exception occurs.")
    exception_type: ExceptionType = Field(description="Indicates whether service is available on the date specified in the date field.")


class PaymentMethod(Enum):
    ON_BOARD = 0
    BEFORE_BOARDING = 1


class FareTransferType(Enum):
    NOT_ALLOWED = 0
    ONCE = 1
    TWICE = 2


class FareAttribute(BaseModel):
    """Fare information for a transit agency's routes."""

    fare_id: Identifier
    price: NonNegativeFloat
    currency_type: str
    payment_methods: PaymentMethod
    transfers: FareTransferType | None = Field(
        description="Indicates the number of transfers permitted on this fare. `None` means unlimited transfers.",
    )
    agency_id: OptionalIdentifier = None
    transfer_duration: NonNegativeInt | None = Field(
        default=None,
        description="Length of time in seconds before a transfer expires. When transfers=0 this field may be used to indicate how long a ticket is valid for or it may be left empty.",
    )


class FareRule(BaseModel):
    """Rules to apply fares for itineraries."""

    fare_id: Identifier
    route_id: OptionalIdentifier = None
    origin_id: OptionalIdentifier = None
    destination_id: OptionalIdentifier = None
    contains_id: OptionalIdentifier = None


class Timeframe(BaseModel):
    """Date and time periods to use in fare rules for fares that depend on date and time factors."""

    timeframe_group_id: Identifier
    start_time: time = Field(default=time(0, 0, 0))
    end_time: time = Field(default=time(23, 59, 59, 999999))
    service_id: Identifier = Field(description="Foreign ID referencing calendar.service_id or calendar_dates.service_id")

    @model_validator(mode="after")
    def check_start_end_time(self) -> Self:
        if self.start_time > self.end_time:
            msg = "start_time cannot be after end_time"
            raise ValueError(msg)
        return self


class FareMediaType(Enum):
    NONE = 0
    PHYSICAL_PAPER_TICKET = 1
    PHYSICAL_TRANSIT_CARD = 2
    CONTACTLESS_EMV = 3
    MOBILE_APP = 4


class FareMedia(BaseModel):
    """To describe the fare media that can be employed to use fare products."""

    fare_media_id: Identifier
    fare_media_name: str = Field(
        default="",
        description="Name of the fare media. For fare media which are transit cards (fare_media_type =2) or mobile apps (fare_media_type =4), the fare_media_name should be included and should match the rider-facing name used by the organizations delivering them.",
    )
    fare_media_type: FareMediaType


class FareProduct(BaseModel):
    """To describe the different types of tickets or fares that can be purchased by riders."""

    fare_product_id: Identifier
    fare_product_name: str = ""
    fare_media_id: OptionalIdentifier = Field(default=None, description="Foreign ID referencing fare_media.fare_media_id")
    amount: float = Field(
        description="The cost of the fare product. May be negative to represent transfer discounts. May be zero to represent a fare product that is free.",
    )
    currency: str


class FareLegRule(BaseModel):
    """
    Fare rules for individual legs of travel.

    <https://gtfs.org/schedule/reference/#fare_leg_rulestxt>_

    """

    leg_group_id: OptionalIdentifier = None
    network_id: OptionalIdentifier = None
    from_area_id: OptionalIdentifier = None
    to_area_id: OptionalIdentifier = None
    from_timeframe_group_id: OptionalIdentifier = None
    to_timeframe_group_id: OptionalIdentifier = None
    fare_product_id: Identifier
    rule_priority: NonNegativeInt | None = None


class DurationLimitType(Enum):
    BETWEEN_DEPARTURE_CURRENT_LEG_ARRIVAL_NEXT_LEG = 0
    BETWEEN_DEPARTURE_CURRENT_LEG_DEPARTURE_NEXT_LEG = 1
    BETWEEN_ARRIVAL_CURRENT_LEG_DEPARTURE_NEXT_LEG = 2
    BETWEEN_ARRIVAL_CURRENT_LEG_ARRIVAL_NEXT_LEG = 3


class FareTransferRuleType(Enum):
    """
    <https://gtfs.org/schedule/reference/#fare_transfer_rulestxt>_
    """

    A_PLUS_AB = 0
    A_PLUS_AB_PLUS_B = 1
    AB = 2


class FareTransferRule(BaseModel):
    """Fare rules for transfers between legs of travel."""

    from_leg_group_id: OptionalIdentifier = None
    to_leg_group_id: OptionalIdentifier = None
    transfer_count: NonNegativeInt | None = None
    duration_limit: NonNegativeInt | None = None
    duration_limit_type: DurationLimitType | None = None
    fare_transfer_type: FareTransferRuleType
    fare_product_id: OptionalIdentifier = None

    def is_same_leg_group(self) -> bool:
        return (self.from_leg_group_id is not None and self.to_leg_group_id is not None) and self.from_leg_group_id == self.to_leg_group_id

    @model_validator(mode="after")
    def check_transfer_count(self) -> Self:
        if self.is_same_leg_group():
            if self.transfer_count is None:
                msg = "transfer_count is required if from_leg_group_id is equal to to_leg_group_id"
                raise ValueError(msg)
        else:
            if self.transfer_count is not None:
                msg = "transfer_count cannot be defined if from_leg_group_id is not equal to to_leg_group_id"
                raise ValueError(msg)
        return self

    @model_validator(mode="after")
    def check_duration_limit_type(self) -> Self:
        if self.duration_limit is not None:
            if self.duration_limit_type is None:
                msg = "duration_limit_type is required if duration_limit is defined"
                raise ValueError(msg)
        else:
            if self.duration_limit_type is not None:
                msg = "duration_limit_type cannot be defined if duration_limit is not defined"
                raise ValueError(msg)
        return self


class Area(BaseModel):
    """Area grouping of locations."""

    area_id: Identifier
    area_name: str = ""


class StopArea(BaseModel):
    """Rules to assign stops to areas."""

    area_id: Identifier
    stop_id: Identifier


class Network(BaseModel):
    """Network grouping of routes."""

    network_id: Identifier
    network_name: str = ""


class RouteNetwork(BaseModel):
    """Rules to assign routes to networks."""

    network_id: Identifier
    route_id: Identifier


class Shape(BaseModel):
    """Rules for mapping vehicle travel paths, sometimes referred to as route alignments."""

    shape_id: Identifier
    shape_pt_lat: float = Field(ge=-90, le=90)
    shape_pt_lon: float = Field(ge=-180, le=180)
    shape_pt_sequence: NonNegativeInt
    shape_dist_traveled: NonNegativeFloat | None = None


class ExactTimes(Enum):
    FREQUENCY_BASED_TRIPS = 0
    SCHEDULE_BASED_TRIPS = 1


class Frequency(BaseModel):
    """Headway (time between trips) for headway-based service or a compressed representation of fixed-schedule service."""

    trip_id: Identifier
    start_time: time
    end_time: time
    headway_secs: PositiveInt
    exact_times: ExactTimes = ExactTimes.FREQUENCY_BASED_TRIPS

    @model_validator(mode="after")
    def check_exact_times(self) -> Self:
        if self.exact_times == ExactTimes.SCHEDULE_BASED_TRIPS and self.start_time > self.end_time:
            msg = "start_time must be less than end_time"
            raise ValueError(msg)
        return self


class TransferType(Enum):
    RECOMMENDED = 0
    TIMED = 1
    MINIMUM_TIME_REQUIRED = 2
    NOT_POSSIBLE = 3
    IN_SEAT = 4
    IN_SEAT_NOT_ALLOWED = 5


class Transfer(BaseModel):
    """Rules for making connections at transfer points between routes."""

    from_stop_id: OptionalIdentifier = None
    to_stop_id: OptionalIdentifier = None
    from_route_id: OptionalIdentifier = None
    to_route_id: OptionalIdentifier = None
    from_trip_id: OptionalIdentifier = None
    to_trip_id: OptionalIdentifier = None
    transfer_type: TransferType = TransferType.RECOMMENDED
    min_transfer_time: NonNegativeInt | None = None

    @model_validator(mode="after")
    def check_stop_ids(self) -> Self:
        if self.transfer_type in (TransferType.IN_SEAT, TransferType.IN_SEAT_NOT_ALLOWED):
            if self.from_stop_id is not None or self.to_stop_id is not None:
                msg = f"from/to_stop_id cannot be defined if transfer_type is {self.transfer_type}"
                raise ValueError(msg)
        elif self.from_stop_id is None or self.to_stop_id is None:
            msg = "from/to_stop_id fields must be defined"
            raise ValueError(msg)
        return self

    @model_validator(mode="after")
    def check_trip_ids(self) -> Self:
        if self.transfer_type in (TransferType.IN_SEAT, TransferType.IN_SEAT_NOT_ALLOWED) and (
            self.from_trip_id is None or self.to_trip_id is None
        ):
            msg = f"from/to_trip_id fields must be defined if transfer_type is {self.transfer_type}"
            raise ValueError(msg)
        return self


class PathwayMode(Enum):
    WALKWAY = 1
    STAIRS = 2
    MOVING_SIDEWALK = 3
    ESCALATOR = 4
    ELEVATOR = 5
    FARE_GATE = 6
    EXIT_GATE = 7


class BiDirectional(Enum):
    UNIDIRECTIONAL = 0
    BIDIRECTIONAL = 1


class Pathway(BaseModel):
    """Pathways linking together locations within stations."""

    pathway_id: Identifier
    from_stop_id: Identifier
    to_stop_id: Identifier
    pathway_mode: PathwayMode
    is_bidirectional: BiDirectional
    length: NonNegativeFloat | None = None
    traversal_time: PositiveInt | None = None
    stair_count: int | None = None
    max_slope: float | None = None
    min_width: PositiveFloat | None = None
    signposted_as: str = ""
    reversed_signposted_as: str = ""

    @model_validator(mode="after")
    def check_is_bidirectional(self) -> Self:
        if self.pathway_mode == PathwayMode.EXIT_GATE and self.is_bidirectional == BiDirectional.BIDIRECTIONAL:
            msg = f"{self.pathway_mode} cannot be {self.is_bidirectional}"
            raise ValueError(msg)
        return self


class Level(BaseModel):
    """Levels within stations."""

    level_id: Identifier
    level_index: float
    level_name: str = ""


class LocationGroup(BaseModel):
    """A group of stops that together indicate locations where a rider may request pickup or drop off."""

    location_group_id: Identifier
    location_group_name: str = ""


class LocationGroupStop(BaseModel):
    """Rules to assign stops to location groups."""

    location_group_id: Identifier
    stop_id: Identifier


class BookingType(Enum):
    REAL_TIME = 0
    UP_TO_SAME_DAY = 1
    UP_TO_PRIOR_DAYS = 2


class BookingRule(BaseModel):
    """Booking information for rider-requested services."""

    booking_rule_id: Identifier
    booking_type: BookingType
    prior_notice_duration_min: NonNegativeInt | None = None
    prior_notice_duration_max: NonNegativeInt | None = None
    prior_notice_last_day: NonNegativeInt | None = None
    prior_notice_last_time: time | None = None
    prior_notice_start_day: NonNegativeInt | None = None
    prior_notice_start_time: time | None = None
    prior_notice_service_id: OptionalIdentifier = Field(default=None, description="ID referencing calendar.service_id")
    message: str = ""
    pickup_message: str = ""
    drop_off_message: str = ""
    phone_number: str = ""
    info_url: AnyUrl | None = None
    booking_url: AnyUrl | None = None

    @model_validator(mode="after")
    def check_prior_notice_duration_min(self) -> Self:
        if self.booking_type == BookingType.UP_TO_SAME_DAY and self.prior_notice_duration_min is None:
            msg = f"prior_notice_duration_min must be defined if booking_type is {self.booking_type}"
            raise ValueError(msg)
        if self.booking_type != BookingType.UP_TO_SAME_DAY and self.prior_notice_duration_min is not None:
            msg = f"prior_notice_duration_min cannot be defined with booking_type {self.booking_type}"
            raise ValueError(msg)
        return self

    @model_validator(mode="after")
    def check_prior_notice_duration_max(self) -> Self:
        if self.booking_type in (BookingType.REAL_TIME, BookingType.UP_TO_PRIOR_DAYS) and self.prior_notice_duration_max is not None:
            msg = f"prior_notice_duration_max cannot be defined if booking_type is {self.booking_type}"
            raise ValueError(msg)
        return self

    @model_validator(mode="after")
    def check_prior_notice_last_day(self) -> Self:
        if self.booking_type == BookingType.UP_TO_PRIOR_DAYS and self.prior_notice_last_day is None:
            msg = f"prior_notice_last_day must be defined if booking_type is {self.booking_type}"
            raise ValueError(msg)
        if self.booking_type != BookingType.UP_TO_PRIOR_DAYS and self.prior_notice_last_day is not None:
            msg = f"prior_notice_duration_min cannot be defined with booking_type {self.booking_type}"
            raise ValueError(msg)
        return self

    @model_validator(mode="after")
    def check_prior_notice_last_time(self) -> Self:
        if self.prior_notice_last_day is not None and self.prior_notice_last_time is None:
            msg = "prior_notice_last_time must be defined if prior_notice_last_day is defined"
            raise ValueError(msg)
        if self.prior_notice_last_day is None and self.prior_notice_last_time is not None:
            msg = "prior_notice_last_time cannot be defined without prior_notice_last_day"
            raise ValueError(msg)
        return self

    @model_validator(mode="after")
    def check_prior_notice_start_day(self) -> Self:
        if self.booking_type == BookingType.REAL_TIME and self.prior_notice_start_day is not None:
            msg = f"prior_notice_start_day cannot be defined if booking_type is {self.booking_type}"
            raise ValueError(msg)
        if self.booking_type == BookingType.UP_TO_SAME_DAY and self.prior_notice_duration_max is not None:
            msg = (
                f"prior_notice_start_day cannot be defined if booking_type is {self.booking_type} and prior_notice_duration_max is defined"
            )
            raise ValueError(msg)
        return self

    @model_validator(mode="after")
    def check_prior_notice_start_time(self) -> Self:
        if self.prior_notice_start_day is not None and self.prior_notice_start_time is None:
            msg = "prior_notice_start_time must be defined if prior_notice_start_day is defined"
            raise ValueError(msg)
        if self.prior_notice_start_day is None and self.prior_notice_start_time is not None:
            msg = "prior_notice_start_time must not be defined if prior_notice_start_day is not defined"
            raise ValueError(msg)
        return self

    @model_validator(mode="after")
    def check_prior_notice_service_id(self) -> Self:
        if self.booking_type != BookingType.UP_TO_PRIOR_DAYS and self.prior_notice_service_id is not None:
            msg = f"prior_notice_service_id must not be defined if booking_type is {self.booking_type}"
            raise ValueError(msg)
        return self


class TableName(Enum):
    AGENCY = "agency"
    STOPS = "stops"
    ROUTES = "routes"
    TRIPS = "trips"
    STOP_TIMES = "stop_times"
    PATHWAYS = "pathways"
    LEVELS = "levels"
    FEED_INFO = "feed_info"
    ATTRIBUTIONS = "attributions"


class Translation(BaseModel):
    """Translations of customer-facing dataset values."""

    table_name: TableName
    field_name: str
    language: str
    translation: str
    record_id: OptionalIdentifier = None
    record_sub_id: OptionalIdentifier = None
    field_value: str = ""

    @model_validator(mode="after")
    def check_record_id(self) -> Self:
        if not self.field_value.strip() and self.record_id is None:
            msg = "record_id is required if field_value is empty"
            raise ValueError(msg)
        if self.field_value and self.record_id is not None:
            msg = "record_id cannot be defined if field_value is defined"
            raise ValueError(msg)
        if self.table_name == TableName.FEED_INFO and self.record_id is not None:
            msg = f"record_id cannot be defined if table_name is {self.table_name}"
            raise ValueError(msg)
        return self

    @model_validator(mode="after")
    def check_record_sub_id(self) -> Self:
        if self.table_name == TableName.STOP_TIMES and self.record_id is not None and self.record_sub_id is None:
            msg = f"record_sub_id must be defined if table_name == {self.table_name} and record_id is defined"
            raise ValueError(msg)
        if self.field_value and self.record_sub_id is not None:
            msg = "record_sub_id cannot be defined if field_value is defined"
            raise ValueError(msg)
        if self.table_name == TableName.FEED_INFO and self.record_sub_id is not None:
            msg = f"record_sub_id cannot be defined if table_name is {self.table_name}"
            raise ValueError(msg)
        return self

    @model_validator(mode="after")
    def check_field_value(self) -> Self:
        if self.field_value.strip():
            if self.table_name == TableName.FEED_INFO:
                msg = f"field_value cannot be defined if table_name is {self.table_name}"
                raise ValueError(msg)
            if self.record_id is not None:
                msg = "field_value cannot be defined if record_id is defined"
                raise ValueError(msg)
        elif self.record_id is None and not self.field_value.strip():
            msg = "field_value must be defined if record_id is not defined"
            raise ValueError(msg)
        return self


class FeedInfo(BaseModel):
    """Dataset metadata, including publisher, version, and expiration information."""

    feed_publisher_name: str
    feed_publisher_url: AnyUrl
    feed_lang: str
    default_lang: str = ""
    feed_start_date: date | None = None
    feed_end_date: date | None = None
    feed_version: str = ""
    feed_contact_email: EmailStr | None = None
    feed_contact_url: AnyUrl | None = None


class OrgRole(Enum):
    DOES_NOT_HAVE_ROLE = 0
    HAS_ROLE = 1


class Attribution(BaseModel):
    """Dataset attributions."""

    attribution_id: OptionalIdentifier
    agency_id: OptionalIdentifier
    route_id: OptionalIdentifier
    trip_id: OptionalIdentifier
    organization_name: str
    is_producer: OrgRole = OrgRole.DOES_NOT_HAVE_ROLE
    is_operator: OrgRole = OrgRole.DOES_NOT_HAVE_ROLE
    is_authority: OrgRole = OrgRole.DOES_NOT_HAVE_ROLE
    attribution_url: AnyUrl | None = None
    attribution_email: EmailStr | None = None
    attribution_phone: str = ""
