from datetime import time

import pytest
from pydantic import ValidationError

from pygtfs.entities import BookingRule, BookingType


def test_up_to_same_day_booking_must_have_prior_notice_duration_min():
    with pytest.raises(ValidationError):
        BookingRule(booking_rule_id="booking-rule", booking_type=BookingType.UP_TO_SAME_DAY)


def test_non_same_day_booking_rule_cannot_have_prior_notice_duration_max():
    for booking_type in (BookingType.REAL_TIME, BookingType.UP_TO_PRIOR_DAYS):
        with pytest.raises(ValidationError):
            BookingRule(
                booking_rule_id="booking-rule",
                booking_type=booking_type,
                prior_notice_start_day=3,
                prior_notice_last_day=4,
                prior_notice_duration_min=10,
            )


def test_up_to_prior_days_booking_rule_must_have_prior_notice_last_day_defined():
    with pytest.raises(ValidationError):
        BookingRule(
            booking_rule_id="booking-rule",
            booking_type=BookingType.UP_TO_PRIOR_DAYS,
        )


def test_prior_notice_last_time_must_be_defined_with_prior_notice_last_day():
    with pytest.raises(ValidationError):
        BookingRule(
            booking_rule_id="booking-rule",
            booking_type=BookingType.UP_TO_PRIOR_DAYS,
            prior_notice_last_day=3,
        )


def test_prior_notice_last_time_cannot_be_defined_without_prior_notice_last_day():
    with pytest.raises(ValidationError):
        BookingRule(booking_rule_id="booking-rule", booking_type=BookingType.UP_TO_PRIOR_DAYS, prior_notice_last_time=time(8))


def test_prior_notice_start_time_must_be_defined_with_prior_notice_start_day():
    with pytest.raises(ValidationError):
        BookingRule(
            booking_rule_id="booking-rule",
            booking_type=BookingType.UP_TO_PRIOR_DAYS,
            prior_notice_last_day=6,
            prior_notice_last_time=time(8),
            prior_notice_start_day=5,
        )


def test_prior_notice_start_time_cannot_be_defined_without_prior_notice_start_day():
    with pytest.raises(ValidationError):
        BookingRule(
            booking_rule_id="booking-rule",
            booking_type=BookingType.UP_TO_PRIOR_DAYS,
            prior_notice_last_day=6,
            prior_notice_last_time=time(8),
            prior_notice_start_time=time(7),
        )


def test_prior_notice_service_id_cannot_be_defined_for_non_up_to_prior_day_booking_types():
    with pytest.raises(ValidationError):
        BookingRule(
            booking_rule_id="booking-rule",
            booking_type=BookingType.UP_TO_SAME_DAY,
            prior_notice_duration_min=30,
            prior_notice_service_id="service-id",
        )
