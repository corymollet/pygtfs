import pytest
from pydantic import ValidationError

from pygtfs.entities import DurationLimitType, FareTransferRule, FareTransferRuleType


@pytest.fixture
def a_plus_ab_rule():
    return FareTransferRule(fare_transfer_type=FareTransferRuleType.A_PLUS_AB)


def test_fare_rule_is_same_leg_group(a_plus_ab_rule):
    a_plus_ab_rule.from_leg_group_id = "some-leg-group"
    a_plus_ab_rule.to_leg_group_id = "some-leg-group"
    assert a_plus_ab_rule.is_same_leg_group()
    a_plus_ab_rule.to_leg_group_id = "some-other-leg-group"
    assert not a_plus_ab_rule.is_same_leg_group()


def test_fare_rule_transfer_count_required_for_same_leg_group():
    with pytest.raises(ValidationError):
        FareTransferRule(
            fare_transfer_type=FareTransferRuleType.A_PLUS_AB,
            from_leg_group_id="some-leg-group",
            to_leg_group_id="some-leg-group",
            transfer_count=None,
        )


def test_fare_rule_transfer_count_forbidden_for_different_leg_group():
    with pytest.raises(ValidationError):
        FareTransferRule(
            fare_transfer_type=FareTransferRuleType.A_PLUS_AB,
            from_leg_group_id="some-leg-group",
            to_leg_group_id="other-leg-group",
            transfer_count=2,
        )


def test_fare_rule_duration_limit_type_required_if_duration_limit_defined():
    with pytest.raises(ValidationError):
        FareTransferRule(fare_transfer_type=FareTransferRuleType.A_PLUS_AB, duration_limit=5)


def test_fare_rule_duration_limit_type_forbidden_if_duration_limit_not_defined():
    with pytest.raises(ValidationError):
        FareTransferRule(
            fare_transfer_type=FareTransferRuleType.A_PLUS_AB,
            duration_limit_type=DurationLimitType.BETWEEN_ARRIVAL_CURRENT_LEG_ARRIVAL_NEXT_LEG,
        )
