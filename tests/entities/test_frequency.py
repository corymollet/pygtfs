from datetime import time

import pytest
from pydantic import ValidationError

from pygtfs.entities import ExactTimes, Frequency


def test_scheduled_based_frequency_invalid_if_start_time_greater_than_end_time():
    with pytest.raises(ValidationError):
        Frequency(
            trip_id="some-trip-id", start_time=time(8), end_time=time(7, 30), headway_secs=10, exact_times=ExactTimes.SCHEDULE_BASED_TRIPS
        )


def test_frequency_based_frequency_valid_if_start_time_greater_than_end_time():
    assert Frequency(
        trip_id="some-trip-id", start_time=time(8), end_time=time(7, 30), headway_secs=10, exact_times=ExactTimes.FREQUENCY_BASED_TRIPS
    )
