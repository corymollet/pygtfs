import pytest
from pydantic import ValidationError

from pygtfs.entities import BiDirectional, Pathway, PathwayMode


def test_exit_gate_pathway_must_be_unidirectional():
    with pytest.raises(ValidationError):
        Pathway(
            pathway_id="pathway",
            from_stop_id="from-stop",
            to_stop_id="to-stop",
            is_bidirectional=BiDirectional.BIDIRECTIONAL,
            pathway_mode=PathwayMode.EXIT_GATE,
        )
