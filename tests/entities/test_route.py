import pytest
from pydantic import ValidationError

from pygtfs.entities import Route, RouteType


def test_route_short_long_name():
    route = Route(route_id="some-route", route_type=RouteType.SUBWAY, route_short_name="Some Subway Route")
    route.route_short_name = ""
    with pytest.raises(ValidationError):
        Route.model_validate(route)
    route.route_long_name = "A much longer subway route description"
    assert Route.model_validate(route)
