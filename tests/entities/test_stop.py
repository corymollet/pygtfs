import pytest
from faker import Faker
from pydantic import ValidationError

from pygtfs.entities import LocationType, Stop

fake = Faker()


@pytest.fixture
def latlng():
    return tuple(map(float, fake.latlng()))


@pytest.fixture
def stop(latlng):
    return Stop(stop_id="some-stop", stop_name="Some Stop", location_type=LocationType.STOP, stop_lat=latlng[0], stop_lon=latlng[1])


@pytest.fixture
def station(latlng):
    return Stop(
        stop_id="some-station", stop_name="Some Station", location_type=LocationType.STATION, stop_lat=latlng[0], stop_lon=latlng[1]
    )


@pytest.fixture
def entrance_exit(latlng, stop):
    return Stop(
        stop_id="some-entrance",
        stop_name="Some Entrance",
        parent_station=stop.stop_id,
        location_type=LocationType.ENTRANCE_EXIT,
        stop_lat=latlng[0],
        stop_lon=latlng[1],
    )


@pytest.fixture
def generic_node(stop):
    return Stop(stop_id="some-stop", parent_station=stop.stop_id, location_type=LocationType.GENERIC_NODE)


@pytest.fixture
def boarding_area(stop):
    return Stop(
        stop_id="some-boarding-area",
        parent_station=stop.stop_id,
        location_type=LocationType.BOARDING_AREA,
    )


def test_stop_name_required_for_stop_location_type(stop):
    stop.stop_name = ""
    with pytest.raises(ValidationError):
        Stop.model_validate(stop)


def test_stop_name_optional_for_boarding_area(boarding_area):
    boarding_area.stop_name = "Some Boarding area"
    assert Stop.model_validate(boarding_area)


def test_certain_location_types_must_have_parent_station_defined(entrance_exit, generic_node, boarding_area):
    entrance_exit.parent_station = None
    with pytest.raises(ValidationError):
        Stop.model_validate(entrance_exit)
    generic_node.parent_station = None
    with pytest.raises(ValidationError):
        Stop.model_validate(generic_node)
    boarding_area.parent_station = None
    with pytest.raises(ValidationError):
        Stop.model_validate(boarding_area)


def test_station_cannot_have_parent_station_defined(station):
    station.parent_station = "some-stop-id"
    with pytest.raises(ValidationError):
        Stop.model_validate(station)
