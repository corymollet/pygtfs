from datetime import time

import pytest
from pydantic import ValidationError

from pygtfs.entities import ContinuousPickupDropOff, PickupDropOff, StopTime, TimePoint


@pytest.fixture
def basic_stop_time():
    return StopTime(trip_id="some-trip")


@pytest.fixture
def exact_timepoint_stop_time(basic_stop_time):
    basic_stop_time.timepoint = TimePoint.EXACT
    return basic_stop_time


@pytest.fixture
def approximate_timepoint_stop_time(basic_stop_time):
    basic_stop_time.timepoint = TimePoint.APPROXIMATE
    return basic_stop_time


@pytest.fixture(scope="module")
def start_window():
    return time(8)


@pytest.fixture(scope="module")
def end_window():
    return time(8, 15)


@pytest.fixture
def pickup_drop_off_stop_time(basic_stop_time, start_window, end_window):
    basic_stop_time.start_pickup_drop_off_window = start_window
    basic_stop_time.end_pickup_drop_off_window = end_window
    return basic_stop_time


@pytest.fixture
def arrival_departure_stop_time(basic_stop_time, start_window, end_window):
    basic_stop_time.arrival_time = start_window
    basic_stop_time.departure_time = end_window
    return basic_stop_time


def test_stop_time_invalid_if_exact_timepoint_no_arrival_departure_times(exact_timepoint_stop_time):
    with pytest.raises(ValidationError):
        StopTime.model_validate(exact_timepoint_stop_time)


def test_stop_time_invalid_if_exact_timepoint_arrival_time_only(exact_timepoint_stop_time, start_window):
    exact_timepoint_stop_time.arrival_time = start_window
    with pytest.raises(ValidationError):
        StopTime.model_validate(exact_timepoint_stop_time)


def test_stop_time_raises_if_exact_timepoint_departure_time_only(exact_timepoint_stop_time, end_window):
    exact_timepoint_stop_time.departure_time = end_window
    with pytest.raises(ValidationError):
        StopTime.model_validate(exact_timepoint_stop_time)


def test_stop_time_does_valid_if_exact_timepoint_arrival_departure_times(exact_timepoint_stop_time, start_window, end_window):
    exact_timepoint_stop_time.arrival_time = start_window
    exact_timepoint_stop_time.departure_time = end_window
    assert StopTime.model_validate(exact_timepoint_stop_time)


def test_stop_time_valid_if_timepoint_approximate_no_arrival_departure_times(approximate_timepoint_stop_time):
    assert StopTime.model_validate(approximate_timepoint_stop_time)


def test_stop_time_valid_if_timepoint_approximate_arrival_time_only(approximate_timepoint_stop_time, start_window):
    approximate_timepoint_stop_time.arrival_time = start_window
    assert StopTime.model_validate(approximate_timepoint_stop_time)


def test_stop_time_valid_if_timepoint_approximate_departure_time_only(approximate_timepoint_stop_time, end_window):
    approximate_timepoint_stop_time.departure_time = end_window
    assert StopTime.model_validate(approximate_timepoint_stop_time)


def test_stop_time_valid_if_timepoint_approximate_arrival_departure_times(approximate_timepoint_stop_time, start_window, end_window):
    approximate_timepoint_stop_time.arrival_time = start_window
    approximate_timepoint_stop_time.departure_time = end_window
    assert StopTime.model_validate(approximate_timepoint_stop_time)


def test_stop_time_invalid_if_one_pickup_drop_off_field_defined_without_complement(basic_stop_time, start_window, end_window):
    basic_stop_time.start_pickup_drop_off_window = start_window
    with pytest.raises(ValidationError):
        assert StopTime.model_validate(basic_stop_time)
    basic_stop_time.start_pickup_drop_off_window = None
    basic_stop_time.end_pickup_drop_off_window = end_window
    with pytest.raises(ValidationError):
        assert StopTime.model_validate(basic_stop_time)


def test_stop_time_valid_if_pickup_drop_off_fields_defined_properly(basic_stop_time, start_window, end_window):
    basic_stop_time.start_pickup_drop_off_window = start_window
    basic_stop_time.end_pickup_drop_off_window = end_window
    assert StopTime.model_validate(basic_stop_time)


def test_stop_time_invalid_if_both_pickup_drop_off_and_arrival_departure_time_fields_defined(basic_stop_time, start_window, end_window):
    basic_stop_time.arrival_time = start_window
    basic_stop_time.departure_time = end_window
    assert StopTime.model_validate(basic_stop_time)
    basic_stop_time.start_pickup_drop_off_window = start_window
    basic_stop_time.end_pickup_drop_off_window = end_window
    with pytest.raises(ValidationError):
        StopTime.model_validate(basic_stop_time)
    basic_stop_time.end_pickup_drop_off_window = None
    with pytest.raises(ValidationError):
        StopTime.model_validate(basic_stop_time)
    basic_stop_time.start_pickup_drop_off_window = time(10)
    with pytest.raises(ValidationError):
        StopTime.model_validate(basic_stop_time)


def test_stop_time_validity_if_pickup_drop_off_fields_defined_with_certain_pickup_types(pickup_drop_off_stop_time):
    pickup_drop_off_stop_time.pickup_type = PickupDropOff.REGULARLY_SCHEDULED
    with pytest.raises(ValidationError):
        StopTime.model_validate(pickup_drop_off_stop_time)
    pickup_drop_off_stop_time.pickup_type = PickupDropOff.MUST_COORDINATE_WITH_DRIVER
    with pytest.raises(ValidationError):
        StopTime.model_validate(pickup_drop_off_stop_time)
    pickup_drop_off_stop_time.pickup_type = PickupDropOff.MUST_CONTACT_AGENCY
    assert StopTime.model_validate(pickup_drop_off_stop_time)


def test_stop_time_validity_if_pickup_types_defined_without_pickup_drop_off_fields(arrival_departure_stop_time):
    arrival_departure_stop_time.pickup_type = PickupDropOff.REGULARLY_SCHEDULED
    assert StopTime.model_validate(arrival_departure_stop_time)
    arrival_departure_stop_time.pickup_type = PickupDropOff.MUST_COORDINATE_WITH_DRIVER
    assert StopTime.model_validate(arrival_departure_stop_time)
    arrival_departure_stop_time.pickup_type = PickupDropOff.MUST_CONTACT_AGENCY
    assert StopTime.model_validate(arrival_departure_stop_time)


def test_pickup_drop_off_stop_time_invalid_if_continuous_pickup_defined(pickup_drop_off_stop_time):
    pickup_drop_off_stop_time.continuous_drop_off = ContinuousPickupDropOff.NO_CONTINUOUS_STOPPING
    with pytest.raises(ValidationError):
        StopTime.model_validate(pickup_drop_off_stop_time)
