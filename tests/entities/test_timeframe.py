from datetime import time

import pytest
from pydantic import ValidationError

from pygtfs.entities import Timeframe


def test_timeframe_start_time_cannot_be_after_end_time():
    timeframe = Timeframe(timeframe_group_id="some-group-id", start_time=time(8), end_time=time(8, 30), service_id="some-service-id")
    timeframe.start_time = time(9)
    with pytest.raises(ValidationError):
        Timeframe.model_validate(timeframe)
