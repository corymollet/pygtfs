import pytest
from pydantic import ValidationError

from pygtfs.entities import Transfer, TransferType


def test_certain_transfer_types_invalid_if_from_to_stops_not_defined():
    for transfer_type in (TransferType.RECOMMENDED, TransferType.TIMED, TransferType.MINIMUM_TIME_REQUIRED, TransferType.NOT_POSSIBLE):
        with pytest.raises(ValidationError):
            Transfer(transfer_type=transfer_type)


def test_in_seat_transfer_invalid_if_from_to_stops_defined():
    with pytest.raises(ValidationError):
        Transfer(transfer_type=TransferType.IN_SEAT, from_stop_id="from-stop", to_stop_id="to-stop")


def test_in_seat_not_allowed_transfer_invalid_if_from_to_stops_defined():
    with pytest.raises(ValidationError):
        Transfer(transfer_type=TransferType.IN_SEAT_NOT_ALLOWED, from_stop_id="from-stop", to_stop_id="to-stop")


def test_in_seat_transfer_invalid_if_from_to_trips_not_defined():
    with pytest.raises(ValidationError):
        Transfer(transfer_type=TransferType.IN_SEAT)
    with pytest.raises(ValidationError):
        Transfer(transfer_type=TransferType.IN_SEAT_NOT_ALLOWED)
