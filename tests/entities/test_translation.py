import pytest
from pydantic import ValidationError

from pygtfs.entities import TableName, Translation


def test_translation_without_field_value_must_have_record_id():
    with pytest.raises(ValidationError):
        Translation(table_name=TableName.STOPS, field_name="stop_name", language="it", translation="fermata")
    Translation(table_name=TableName.STOPS, field_name="stop_name", language="it", translation="fermata", record_id="record-id")


def test_translation_with_field_value_cannot_have_record_id():
    with pytest.raises(ValidationError):
        Translation(
            table_name=TableName.STOPS,
            field_name="stop_name",
            language="it",
            translation="fermata",
            field_value="translated-field",
            record_id="record-id",
        )
    Translation(table_name=TableName.STOPS, field_name="stop_name", language="it", translation="fermata", field_value="translated-field")


def test_feed_info_cannot_have_record_id_sub_id_or_field_value():
    with pytest.raises(ValidationError):
        Translation(table_name=TableName.FEED_INFO, field_name="stop_name", language="it", translation="fermata", record_id="record-id")
    with pytest.raises(ValidationError):
        Translation(
            table_name=TableName.FEED_INFO, field_name="stop_name", language="it", translation="fermata", field_value="translated-field"
        )
    with pytest.raises(ValidationError):
        Translation(table_name=TableName.FEED_INFO, field_name="stop_name", language="it", translation="fermata", record_sub_id="sub-id")


def test_stop_time_must_have_record_sub_id():
    with pytest.raises(ValidationError):
        Translation(
            table_name=TableName.STOP_TIMES, field_name="field-name", language="lang", translation="translation", record_id="record-id"
        )
    Translation(
        table_name=TableName.STOP_TIMES,
        field_name="field-name",
        language="lang",
        translation="translation",
        record_id="record-id",
        record_sub_id="sub-id",
    )


def test_record_sub_id_cannot_be_defined_with_field_value():
    with pytest.raises(ValidationError):
        Translation(
            table_name=TableName.STOPS,
            field_name="stop_name",
            language="it",
            translation="fermata",
            field_value="field-value",
            record_sub_id="sub-id",
        )
    Translation(
        table_name=TableName.STOPS, field_name="stop_name", language="it", translation="fermata", record_id="record", record_sub_id="sub-id"
    )


def test_translation_without_record_id_must_have_field_value():
    with pytest.raises(ValidationError):
        Translation(table_name=TableName.STOPS, field_name="stop_name", language="it", translation="fermata")
    Translation(table_name=TableName.STOPS, field_name="stop_name", language="it", translation="fermata", field_value="field-value")
